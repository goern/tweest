scalaVersion := "2.11.11"
scalaVersion in ThisBuild := "2.11.11"

name := "tweest"
organization := "net.b4mad"
version := "0.1.0"

resolvers += "Will's bintray" at "https://dl.bintray.com/willb/maven/"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"
libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.2.0"
libraryDependencies += "io.radanalytics" %% "silex" % "0.2.0"

// To learn more about multi-project builds, head over to the official sbt
// documentation at http://www.scala-sbt.org/documentation.html
