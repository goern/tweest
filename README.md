# Tweest

This is a Twitter Stream API to Kafka to Spark thing.

## Prerequisits

You need a unpacked Apache Kafka and Apache Spark. Furthermore you need to run Kafka, see the [Quickstart](https://kafka.apache.org/quickstart) for instructions.

## Running

Lets start the first component: twitter to kafka:

```bash
pipenv install
pipenv shell
./src/main/python/twitter2kafka.py
```

On a second terminal, start the spark processor:

```bash
spark-submit --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0 ./src/main/python/spark_consumer.py
```
