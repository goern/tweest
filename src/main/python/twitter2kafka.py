#!/usr/bin/env python3
#
# This file is part of tweest.  tweest is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright 2017, Christoph Görn

"""This is tweest, a gateway from Twitter's streaming API to Apache Kafka
"""

import os
from os.path import join, dirname
import logging
import json

import tweepy

from dotenv import load_dotenv
from kafka import KafkaProducer
from kafka.errors import KafkaError
from prometheus_client import Histogram, start_http_server

__author__ = "Christoph Görn"
__copyright__ = "Copyright 2017, Christoph Görn"

__license__ = "GPLv3"
__version__ = "0.1.0"
__maintainer__ = "Christoph Görn"
__email__ = "goern@sb4mad.net"

# check if we get deployed to something else than DEV
if os.getenv('ENVIRONMENT', 'DEV') == 'DEV':
    # if we are in DEV, load ENV from .env
    DOTENV_PATH = join(dirname(__file__), '.env')

    load_dotenv(DOTENV_PATH, verbose=True)

# lets configure the application
TWITTER_CONSUMER_KEY = os.getenv('TWITTER_CONSUMER_KEY')
TWITTER_CONSUMER_SECRET = os.getenv('TWITTER_CONSUMER_SECRET')
TWITTER_ACCESS_TOKEN = os.getenv('TWITTER_ACCESS_TOKEN')
TWITTER_ACCESS_TOKEN_SECRET = os.getenv('TWITTER_ACCESS_TOKEN_SECRET')
KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_PEERS', 'localhost:9092')
KAFKA_TWITTER_TOPIC = os.getenv('KAFKA_TOPIC', 'twitter.status')


statusTextHistogram = Histogram(
    'twitter_status_bytes', 'Histogram of Twitter Statuses sizes (bytes)',
    buckets=(8, 10, 12, 16, 32, 64, 70, 75, 80, 90, 100, 115, 120, 128, 130, 135, 140.0))


class StreamListener(tweepy.StreamListener):
    def __init__(self):
        self.logger = logging.getLogger('StreamListener')

        super(StreamListener, self).__init__()

        self.eventProducer = KafkaProducer(
            bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
            value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def on_status(self, status):
        try:
            self.eventProducer.send(KAFKA_TWITTER_TOPIC, status)
            statusTextHistogram.observe(len(status.text))
        except KafkaError as t:
            self.logger.error(t)
        except Exception as e:
            self.logger.warn(e)

    def on_error(self, status_code):
        if status_code == 420:  # we have been rate limited!
            # returning False in on_data disconnects the stream
            return False


def main():
    auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)

    streamListener = StreamListener()
    stream = tweepy.Stream(auth=api.auth, listener=streamListener)

    user = api.get_user('goern')

    follow_list = []
    for friend in user.friends():
        follow_list.append(friend.id_str)

    track_list = ['kubernetes', 'openshift', 'red hat',
                  'redhat', 'fedora', 'centos', 'atomic host', 'projectatomic']

    start_http_server(8080)

    stream.filter(follow_list, track_list)


if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO)

    main()
