#!/usr/bin/env python3
#
# This file is part of tweest.  tweest is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright 2017, Christoph Görn

"""
Counts words in UTF8 encoded...

Run this using `spark-submit --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0 spark_consumer.py`
"""

import os
import json

os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0'

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils


def createStreamingContext():
    sc = SparkContext(appName="PythonStreamingKafkaTwitterConsumer")
    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, 5)

    kafkaStream = KafkaUtils.createStream(
        ssc, 'localhost:2181', "spark-streaming-consumer", {'twitter.status': 1})

    # parsed = kafkaStream.map(lambda v: json.loads(v[1]))
    parsed = kafkaStream.map(lambda v: json.loads(v[1].encode('utf-8')))

    count_this_batch = kafkaStream.count().map(
        lambda x: ('Tweets this batch: %s' % x))
    count_windowed = kafkaStream.countByWindow(60, 5).map(
        lambda x: ('Tweets total (One minute rolling count): %s' % x))

    count_this_batch.union(count_windowed).pprint()

    authors_dstream = parsed.map(lambda tweet: tweet['user']['screen_name'])
    count_values_this_batch = authors_dstream.countByValue().transform(lambda rdd: rdd.sortBy(
        lambda x: -x[1])).map(lambda x: "Author counts this batch:\tValue %s\tCount %s" % (x[0], x[1]))
    count_values_windowed = authors_dstream.countByValueAndWindow(60, 5).transform(lambda rdd: rdd.sortBy(
        lambda x: -x[1])).map(lambda x: "Author counts (One minute rolling):\tValue %s\tCount %s" % (x[0], x[1]))

    count_values_this_batch.pprint(5)
    count_values_windowed.pprint(5)

    text_dstream = parsed.map(lambda tweet: tweet['text'].encode('utf-8'))
    word_count = text_dstream.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(lambda a, b: a + b)

    word_count.pprint()

    return ssc


def main():
    ssc = StreamingContext.getOrCreate(
        '/tmp/checkpoint', lambda: createStreamingContext())
    ssc.start()
    ssc.awaitTermination()


if __name__ == '__main__':
    main()
