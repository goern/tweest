#!/usr/bin/env python3
#
# This file is part of tweest.  tweest is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright 2017, Christoph Görn

import logging
import json

from kafka import KafkaConsumer
from textwrap import TextWrapper

KAFKA_BOOTSTRAP_SERVERS = 'localhost:9092'
KAFKA_TWITTER_TOPIC = 'twitter.status'


def main():
    consumer = KafkaConsumer(
        bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS, auto_offset_reset='earliest', value_deserializer=lambda v: json.loads(v))

    consumer.subscribe(KAFKA_TWITTER_TOPIC)

    for message in consumer:
        print(message)


if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO)

    main()
