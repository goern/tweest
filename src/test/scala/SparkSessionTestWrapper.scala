package net.b4mad.tweest

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("Tweest Spark Tests")
      .getOrCreate()
  }

}